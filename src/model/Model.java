package model;


import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Model {
    // FINALS
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;
    private static int score;
    // Eigenschaften
    private List<Apfel> aepfel = new LinkedList<>();
    private List<Bombe> bomben = new LinkedList<>();
    private Korb spielerKorb;


    // Konstruktoren
    public Model() {
        this.spielerKorb = KorbFactory.makeKorb();
        this.aepfel.add(ApfelFactory.makeApfel());
        this.bomben.add(BombenFactory.makeBombe());
        score = 0;


    }

    public static int getScore() {
        return score;
    }

    // Methoden
    public void update(long elapsedTime) {
        if (CollusionTest.doCollusionTestApfel(aepfel, spielerKorb)) {
            score++;


        } else if (CollusionTest.doCollusionTestBombe(bomben, spielerKorb)) {
            System.exit(1000);
        }
        for (Apfel apfel : aepfel) {
            apfel.update(elapsedTime);
        }
        for (Bombe bombe : bomben) {
            bombe.update(elapsedTime);
        }
        Random random = new Random();
        if ((random.nextInt(1000) + 1) % 37 == 0)
            aepfel.add(ApfelFactory.makeApfel());
        if ((random.nextInt(1000) + 1) % 97 == 0)
            bomben.add(BombenFactory.makeBombe());


    }

    // Setter + Getter
    public List<Bombe> getBomben() {
        return bomben;
    }

    public List<Apfel> getAepfel() {
        return aepfel;
    }

    public Korb getSpielerKorb() {
        return spielerKorb;
    }
}

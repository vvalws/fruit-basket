package model;

import java.util.List;

public class CollusionTest {


    public static boolean doCollusionTestApfel(List<Apfel> aepfel, Korb spielerKorb) {


        for (Apfel apfel : aepfel) {
            if (apfel.getBoundShape().intersects(spielerKorb.getBoundShape())) {
                return true;

            }

        }
        return false;
    }
    public static boolean doCollusionTestBombe(List<Bombe> bomben, Korb spielerKorb) {


        for (Bombe bombe : bomben) {
            if (bombe.getBoundShape().intersects(spielerKorb.getBoundShape())) {
                return true;

            }

        }
        return false;

    }

}

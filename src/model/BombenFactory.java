package model;

import java.util.Random;

/**
 * Generiert eine Bombe.
 * Startpunkt ist gesetzt am Oberen Rand des Spielfeldes.
 * Bombendurchmesser liegt bei 10.
 */
public class BombenFactory {
    public static Bombe makeBombe() {
        Random random = new Random();
        Punkt2D mittelPunkt = new Punkt2D((random.nextInt(1000) + 1), 1);
        Bombe bombe = new Bombe(mittelPunkt, 35, 0.125f);
        return bombe;
    }
}

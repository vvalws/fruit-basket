package model;



import java.util.Random;

/**
 * Generiert einen Apfel.
 * Startpunkt ist gesetzt am Oberen Rand des Spielfeldes.
 * Apfeldurchmesser liegt bei 100.
 */

public  class ApfelFactory {

    public static Apfel makeApfel() {
        Random random = new Random();
        Punkt2D mittelPunkt = new Punkt2D(random.nextInt(1000), 1);
        Apfel apfel = new Apfel(mittelPunkt, 35, 0.125f);

        return apfel;
    }
}

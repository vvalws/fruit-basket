package model;
/**
 * Apfel-Spielfigur  ist
 */

import java.awt.*;

public class Apfel implements SpielObjekte {


    // Eigenschaften
    private Punkt2D mittelPunkt;
    private int radius;
    private float speed;


    //Konstruktor


    public Apfel(Punkt2D mittelPunkt, int r, float speed) {

        this.mittelPunkt = mittelPunkt;
        this.radius = r;
        this.speed = speed;
    }

    //Getter

    @Override
    public int getRadius() {
        return radius;
    }


    @Override
    public float getSpeed() {
        return speed;
    }

    @Override
    public Punkt2D getMittelPunkt() {
        return mittelPunkt;
    }


    //Methoden
    @Override

    public void update(long elapsedTime) {

        this.mittelPunkt.setY(Math.round(this.mittelPunkt.getY() + elapsedTime * speed));
    }

    @Override
    public Rectangle getBoundShape() {

        return new Rectangle(this.mittelPunkt.getX(),
                this.mittelPunkt.getY(),
                this.getRadius(),
                this.getRadius());
    }
}


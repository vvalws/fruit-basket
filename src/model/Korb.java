package model;

import java.awt.*;

/**
 * Spielerkorb. Der Spieler Navigiert den Spielerkorb mit den Pfeiltasten der Tastatur.
 * Dar Korb kann nach links oder Rechts navigiert werden.
 */
public class Korb  {
    //Eigenschaften
    private Punkt2D mittelPunkt;
    private int hoehe;
    private int breite;

    //Konstruktor
    public Korb(Punkt2D mittelPunkt,int hoehe, int breite) {
        this.mittelPunkt = mittelPunkt;
        this.hoehe = hoehe;
        this.breite = breite;
    }

    //Getter
    public int getHoehe() {
        return hoehe;
    }

    public int getBreite() {
        return breite;
    }

    public Punkt2D getMittelPunkt() {
        return mittelPunkt;

    }


    public void move(int dx, int dy) {
        this.mittelPunkt.setX(this.mittelPunkt.getX()+ dx);
        this.mittelPunkt.setY(this.mittelPunkt.getY()+dy);

    }
    public Rectangle getBoundShape() {

        return new Rectangle(this.mittelPunkt.getX(),
                            this.mittelPunkt.getY(),
                            this.getHoehe(),
                            this.getBreite());
    }

    }

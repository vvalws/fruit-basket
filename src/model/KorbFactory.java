package model;

/**
 * Generiert den Spielerkorb. Spielerkorb befindet sind am unteren Rand des Spielfeldes, mittig.
 */
public class KorbFactory {

    public static Korb makeKorb(){
        Punkt2D punkt = new Punkt2D(500,575);
        Korb korb = new Korb(punkt,30,100);
        return korb;
    }
}

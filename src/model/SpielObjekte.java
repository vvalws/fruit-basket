package model;

import java.awt.*;


/**
 * Definiert die Grundstrucktur aller Spielfiguren im Spiel.
 * Kreise
 * explodieren j/n
 * Haben Mittelpunkt (Objektabhängigkeit)
 * FAllgeschwindigkeit
 */


public interface SpielObjekte {


    int getRadius();


    float getSpeed();

    Punkt2D getMittelPunkt();


    void update(long elapsedTime);

   Rectangle getBoundShape();

}

package model;

/**
 * beschreibt die Koordinaten eines einzelenen Punktes auf dem Canvas.
 * Wird bei der Kostruktion und Bewegung von SpielObjekten und des SpielerKorbes genutzt.
 */
public class Punkt2D {
    //Eigenscahften -> x und y Koordinaten des Punktes
    private int x;

    private int y;

    //Konstruktoren

    public Punkt2D() {
    }

    public Punkt2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}

import model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {
    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler (Model model) {
        this.model = model;
    }

    // Methoden
    public void onKeyPressed(KeyCode key) {



        if(key == KeyCode.LEFT) {
            if(model.getSpielerKorb().getMittelPunkt().getX()+15 >990||model.getSpielerKorb().getMittelPunkt().getX()+15<10){
                model.getSpielerKorb().getMittelPunkt().setX(990);

            }
            model.getSpielerKorb().move(-20,0);
        }
        else if(key == KeyCode.RIGHT) {
            if(model.getSpielerKorb().getMittelPunkt().getX()+15 >990||model.getSpielerKorb().getMittelPunkt().getX()+15<10){
                model.getSpielerKorb().getMittelPunkt().setX(20);

            }
            model.getSpielerKorb().move(20,0);
        }
    }


}

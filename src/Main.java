import model.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class Main extends Application {


    // Eigenschaften initialisieren
    private Timer timer;

    // Konstruktoren

    // Methoden
    public void start(Stage stage) throws Exception {



        /* javaFX Vorbereitungen / Stage
        /////////////////////////////////*/

        // Canvas
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);

        // Group
        Group group = new Group();
        group.getChildren().add(canvas);

        //Scene
        Scene scene = new Scene(group);

        // Stage
        stage.setScene(scene);
        stage.show();

        // Draw
        GraphicsContext gc = canvas.getGraphicsContext2D();
        GraphicsContext sc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model,gc,sc);
        timer = new Timer(model, graphics);
        timer.start();


        // InputHandler
       InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())


        );

    }

    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }

}



import model.*;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;


public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext gc;
    private GraphicsContext sc;

    // Konstruktoren
    public Graphics(Model model, GraphicsContext gc,GraphicsContext sc) {
        this.model = model;
        this.gc = gc;
        this.sc = sc;
    }

    // Methoden
    public void draw() {

        // Clear Screen
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

       gc.setFill(Color.LIGHTBLUE);
       gc.fillRect(0,0,Model.WIDTH,Model.HEIGHT);

        //Draw Score
        sc.setFill(Color.BLUE);
        sc.fillText("Score: "+ this.model.getScore(),50,50);




        // Draw Aepfel
        for (Apfel apfel : this.model.getAepfel()) {
            gc.setFill(Color.RED);
            gc.fillOval(
                    apfel.getMittelPunkt().getX() - apfel.getRadius(),
                    apfel.getMittelPunkt().getY() - apfel.getRadius(),
                    apfel.getRadius() * 2,
                    apfel.getRadius() * 2
            );


        }
        // Draw Bombe
        for (Bombe bombe : this.model.getBomben()) {
            gc.setFill(Color.BLACK);
            gc.fillOval(
                    bombe.getMittelPunkt().getX() - bombe.getRadius(),
                    bombe.getMittelPunkt().getY() - bombe.getRadius(),
                    bombe.getRadius() * 2,
                    bombe.getRadius() * 2
            );

        }

        // Draw SpielerKorb
        gc.setFill(Color.BLANCHEDALMOND);
        gc.fillRect(
                model.getSpielerKorb().getMittelPunkt().getX() - model.getSpielerKorb().getBreite()/2 ,
                model.getSpielerKorb().getMittelPunkt().getY() - model.getSpielerKorb().getHoehe()/2 ,
                model.getSpielerKorb().getBreite(),
                model.getSpielerKorb().getHoehe()
        );


    }
}
